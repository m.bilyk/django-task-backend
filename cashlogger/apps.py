from django.apps import AppConfig


class CashloggerConfig(AppConfig):
    name = 'cashlogger'
