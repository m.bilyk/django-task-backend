from django.conf.urls import url

from . import views

urlpatterns = [
    url('', views.CategoryView.as_view(), name='category')
]