from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializer import CategorySerializer
from ..models import Category
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token


class CategoryView(APIView):

    def get(self, request, format='json'):
        auth_user = request.user
        category = Category.objects.filter(user=auth_user)
        serializer = CategorySerializer(category, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format='json'):
        data = request.data
        serializer = CategorySerializer(data=request.data)
        if serializer.is_valid():
            new_category = Category.objects.create(name=data['name'], balance=data['balance'])
            new_category.user.add(request.user)
            new_category.save()
            check_data = CategorySerializer(new_category)
            return Response(check_data.data, status=status.HTTP_201_CREATED)
