from rest_framework import serializers
from cashlogger.models import Expense
from rest_framework.authtoken.models import Token
from ..category.serializer import CategorySerializer




class ExpenseSerializer(serializers.ModelSerializer):
    category = CategorySerializer()
    class Meta:
        model = Expense
        fields = ("id", "name", "amount_money", "insert_date", "insert_time", "category")