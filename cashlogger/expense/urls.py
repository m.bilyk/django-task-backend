from django.conf.urls import url

from . import views

urlpatterns = [
    url('', views.ExpenseView.as_view(), name='expense')
]