from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializer import ExpenseSerializer
from ..models import Expense


class ExpenseView(APIView):
    def get(self, request, format='json'):
        expense = Expense.objects.filter(category__user=request.user)
        serializer = ExpenseSerializer(expense, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format='json'):
        pass