from django.db import models
from django.contrib.auth.models import User, Group, Permission
# Create your models here.

class Category(models.Model):
    id = models.AutoField(models.IntegerField, primary_key=True)
    name = models.CharField(max_length=120, null=False)
    balance = models.IntegerField(null=False)
    user = models.ManyToManyField(User)


    def __str__(self):
        return self.name

class Expense(models.Model):
    id = models.AutoField(models.IntegerField, primary_key=True)
    name = models.CharField(max_length=60, null=False)
    amount_money = models.IntegerField(null=False)
    insert_date = models.DateField(auto_now=True)
    category = models.ForeignKey(
        'Category',
        on_delete=models.CASCADE,
        null=True
    )
    insert_time = models.TimeField(auto_now=True)

    def __str__(self):
        return self.name


